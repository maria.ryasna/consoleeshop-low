﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public interface ICustomer
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Autorization(string login, string password);
        public bool LoginExist(string login);
    }
}
