﻿using System;
using System.Collections.Generic;
using System.Text;
using PhotoCLub.DataBaseAll;

namespace PhotoCLub
{
    public abstract class Role
    {
        public DataBase Db { get; set; }
        public IPerson CurrentRole { get; set; }
        /// <summary>
        /// Abstract method Menu for different Roles like AdminRole, UserRole and GuestRole
        /// </summary>
        public void ConnectionDB(DataBase db)
        {
            Db = db;
        }
        public abstract Role Menu();
        /// <summary>
        /// Method which return all products for all Roles
        /// </summary>
        public List<ProductBase> ReturnAllProducts()
        {
            List<ProductBase> templist = new List<ProductBase>();
            foreach (var element in Db.DataBaseProduct)
            {
                templist.Add(element);
            }
            return templist;
        }
        /// <summary>
        /// Method which return some product for all Roles by searching
        /// </summary>
        public ProductBase ReturnProductByName(string name)
        {
            List<ProductBase> templist = new List<ProductBase>();
            if (Db.DataBaseProduct.SearchByName(name) != null)
                return Db.DataBaseProduct.SearchByName(name);
            return null;
        }
    }
}
