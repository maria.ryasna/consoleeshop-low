﻿using System;
using System.Collections.Generic;
using System.Text;
using PhotoCLub.DataBaseAll;

namespace PhotoCLub
{
    public class AdminRole : Role
    {
        public new Admin CurrentRole { get; set; }
        public AdminRole(Admin currentrole, DataBase db)
        {
            CurrentRole = currentrole;
            ConnectionDB(db);
        }
        public void CreateNewOrder(string userlogin, string nameproduct)
        {
            if (Db.DataBaseUser.UserExistByLogin(userlogin) && Db.DataBaseProduct.ProductExistByName(nameproduct))
            {
                Order NewOrder = new Order(Db.DataBaseProduct.SearchByName(nameproduct));
                if (Db.DataBaseUser.SearchByLogin(userlogin) as Admin == null)
                    (Db.DataBaseUser.SearchByLogin(userlogin) as User).ListOfOrders.AddOrder(NewOrder);
                else
                    throw new UserNullException("Admin can not create order for youself");
            }
        }
        public Role Logout()
        {
            return new GuestRole(Db);
        }
        public void UserStatusChange(int id, string login, StatusType type)
        {
            if (type == StatusType.CanceledByUser || type == StatusType.Received)
                throw new UserNullException("Admin can not set status 'Canceled by user' or 'Received'");
            if (Db.DataBaseUser.SearchByLogin(login) as Admin == null)
                (Db.DataBaseUser.SearchByLogin(login) as User).ChangeStatusOfOrder(id, type);
            else
                throw new UserNullException("Admin can not create order for youself and change status for this");
        }
        public void AddNewProduct(string nameproduct, int price, CategoryType category)
        {
            ProductBase newproduct;
            if (category == CategoryType.Camera)
                newproduct = new Camera(nameproduct, price);
            else if (category == CategoryType.Len)
                newproduct = new Len(nameproduct, price);
            else throw new ProductNullException("Information incorrect");
            Db.DataBaseProduct.AddProduct(newproduct);
        }
        public void ChangeUserInfo(ICustomer previous, ICustomer current)
        {
            Db.DataBaseUser.ChangeInfo(previous, current);
        }
        public override Role Menu()
        {
            Console.WriteLine("Admin role");
            Console.WriteLine("Welcome to our internet store PhotoCLub, admin {0}!", (CurrentRole).Name);
            Console.WriteLine("Menu:");
            Console.WriteLine("1 - Пересмотреть товары");
            Console.WriteLine("2 - Поиск товара по названию");
            Console.WriteLine("3 - Создание нового заказа");
            Console.WriteLine("4 - Оформление заказа");
            Console.WriteLine("5 - Просмотр и изменение персональной информации пользователя");
            Console.WriteLine("6 - Добавление нового товара");
            Console.WriteLine("7 - Изменение информации про товар");
            Console.WriteLine("8 - Изменение статуса заказа");
            Console.WriteLine("9 - Выход из аккаунта");
            Console.WriteLine("0 - Закрыть сайт");
            string m = Console.ReadLine();
            if (m == "")
                return this;
            int p = Convert.ToInt32(m);
            switch (p)
            {
                case 1:
                    Console.WriteLine("| Name        | Price      | Category     |");
                    foreach (var element in ReturnAllProducts())
                    {
                        Console.WriteLine("| {0}    | {1}    | {2}    |",
                        element.Name, element.Price, element.Category);
                    }
                    Console.ReadKey();
                    return this;
                case 2:
                    Console.WriteLine("Введите название товара, который интересует");
                    string name = Console.ReadLine();
                    if (name == "")
                        return this;
                    if (ReturnProductByName(name) != null)
                        Console.WriteLine("{0} - {1} - {2}",
                        ReturnProductByName(name).Name,
                        ReturnProductByName(name).Price,
                        ReturnProductByName(name).Category);
                    else
                        Console.WriteLine("It isn't exist product with which name");
                    Console.ReadKey();
                    return this;
                case 3:
                    Console.WriteLine("Choose user login for make order");
                    foreach (var user in Db.DataBaseUser)
                    {
                        if (user as Admin == null)
                            Console.WriteLine("{0} - {1} - {2}", user.Name, user.Login, user.Password);
                    }
                    string userlogin = Console.ReadLine();
                    if (userlogin == "")
                        return this;
                    Console.WriteLine("Choose product name");
                    foreach (var product in Db.DataBaseProduct)
                        Console.WriteLine("{0} - {1} - {2}", product.Name,
                                product.Price,
                                product.Category);
                    string nameproduct = Console.ReadLine();
                    if (nameproduct == "")
                        return this;
                    CreateNewOrder(userlogin, nameproduct);
                    Console.WriteLine("Order succesful created");
                    return this;
                case 4:
                    Console.WriteLine("User orders:");
                    foreach (var element in Db.DataBaseUser)
                    {
                        if (element as Admin == null && (element as User).UserHaveAnyOrders())
                        {
                            Console.WriteLine("User {0} orders:", element.Name);
                            foreach (var el in (element as User).ListOfOrders)
                                Console.WriteLine("{0} - {1} -{2}", el.Item1, el.Item2.Product.Name, el.Item2.Status);
                        }
                    }
                    Console.ReadKey();
                    return this;
                case 5:
                    foreach (var element in Db.DataBaseUser)
                    {
                        if (element as Admin == null)
                        {
                            Console.WriteLine("User: {0} - {1} - {2}", element.Name, element.Login, element.Password);
                            foreach (var el in (element as User).ListOfOrders)
                                Console.WriteLine("{0} - {1} -{2}", el.Item1, el.Item2.Product.Name, el.Item2.Status);
                        }
                    }
                    Console.WriteLine("Enter user login for changes");
                    string loguser = Console.ReadLine();
                    if (loguser == "")
                        return this;
                    Console.WriteLine("1 - изменить имя");
                    Console.WriteLine("2 - изменить логин");
                    Console.WriteLine("3 - изменить пароль");
                    string schok = Console.ReadLine();
                    if (schok == "")
                        return this;
                    int punkt = Convert.ToInt32(schok);
                    switch (punkt)
                    {
                        case 1:
                            Console.WriteLine("Введите новое имя");
                            string newname = Console.ReadLine();
                            if (newname == "")
                                return this;
                            User previous = Db.DataBaseUser.SearchByLogin(loguser) as User;
                            User current = new User(newname, previous.Login, previous.Password);
                            ChangeUserInfo(previous, current);
                            Console.WriteLine("Completely changed!");
                            return this;
                        case 2:
                            Console.WriteLine("Введите новый логин");
                            string newlogin = Console.ReadLine();
                            if (newlogin == "")
                                return this;
                            User previouss = Db.DataBaseUser.SearchByLogin(loguser) as User;
                            User currentt = new User(previouss.Name, newlogin, previouss.Password);
                            ChangeUserInfo(previouss, currentt);
                            Console.WriteLine("Completely changed!");
                            return this;
                        case 3:
                            Console.WriteLine("Введите новый пароль");
                            string newpassword = Console.ReadLine();
                            if (newpassword == "")
                                return this;
                            User previousss = Db.DataBaseUser.SearchByLogin(loguser) as User;
                            User currenttt = new User(previousss.Name, previousss.Login, newpassword);
                            ChangeUserInfo(previousss, currenttt);
                            Console.WriteLine("Completely changed!");
                            Console.ReadKey();
                            return this;
                        default:
                            return this;
                    }
                case 6:
                    Console.WriteLine("What type of product do you want to add?");
                    Console.WriteLine("0 - Camera");
                    Console.WriteLine("1 - Len");
                    string categ = Console.ReadLine();
                    if (categ == "")
                        return this;
                    CategoryType category = (CategoryType)Enum.ToObject(typeof(CategoryType), Convert.ToInt32(categ));
                    Console.WriteLine("Enter name of product");
                    string nameofproduct = Console.ReadLine();
                    if (nameofproduct == "")
                        return this;
                    Console.WriteLine("Enter price");
                    string pricestring = Console.ReadLine();
                    if (pricestring == "")
                        return this;
                    int price = Convert.ToInt32(pricestring);
                    AddNewProduct(nameofproduct, price, category);
                    Console.WriteLine("Product successful added!");
                    Console.ReadKey();
                    return this;
                case 7:
                    Console.WriteLine("All products:");
                    foreach (var product in Db.DataBaseProduct)
                        Console.WriteLine("{0} - {1} - {2}", product.Name,
                                product.Price,
                                product.Category);

                    Console.WriteLine("Select product name:");
                    string productname = Console.ReadLine();

                    if (productname == "")
                        return this;
                    Console.WriteLine("ENter new name:");
                    string newnamep = Console.ReadLine();
                    if (newnamep == "")
                        return this;
                    Console.WriteLine("Enter new price");
                    string newprice = Console.ReadLine();
                    if (newprice == "")
                        return this;
                    int newpriceint = Convert.ToInt32(newprice);
                    Console.WriteLine("Select new category:");
                    Console.WriteLine("0 - Camera");
                    Console.WriteLine("1 - Len");
                    string newcateg = Console.ReadLine();
                    if (newcateg == "")
                        return this;
                    CategoryType newcategory = (CategoryType)Enum.ToObject(typeof(CategoryType), Convert.ToInt32(newcateg));
                    ProductBase previousp = Db.DataBaseProduct.SearchByName(productname);
                    if (newcategory == CategoryType.Camera)
                    {
                        ProductBase currentp = new Camera(newnamep, newpriceint);
                        Db.DataBaseProduct.ChangeInfo(previousp, currentp);
                        Console.WriteLine("Product successful changed!");
                    }
                    else if (newcategory == CategoryType.Len)
                    {
                        ProductBase currentp = new Len(newnamep, newpriceint);
                        Db.DataBaseProduct.ChangeInfo(previousp, currentp);
                        Console.WriteLine("Product successful changed!");
                    }

                    Console.ReadKey();
                    return this;
                case 8:
                    foreach (var element in Db.DataBaseUser)
                    {
                        if (element as Admin == null && (element as User).ListOfOrders != null)
                        {
                            Console.WriteLine("User {0} orders:", element.Name);
                            foreach (var el in (element as User).ListOfOrders)
                                Console.WriteLine("{0} - {1} -{2}", el.Item1, el.Item2.Product.Name, el.Item2.Status);
                        }
                    }
                    Console.WriteLine("Enter user login for changes order status");
                    string loginuserr = Console.ReadLine();
                    if (loginuserr == "")
                        return this;
                    Console.WriteLine("Enter id of order");
                    string messegee = Console.ReadLine();
                    if (messegee == "")
                        return this;
                    int ide = Convert.ToInt32(messegee);
                    Console.WriteLine("Choose new status:");
                    Console.WriteLine("1 - CanceledByAdmin");
                    Console.WriteLine("2 - PaymentReceived");
                    Console.WriteLine("3 - Sent");
                    Console.WriteLine("4 - Completed");
                    string state = Console.ReadLine();
                    if (state == "")
                        return this;
                    StatusType statuse = (StatusType)Enum.ToObject(typeof(StatusType), Convert.ToInt32(state));
                    UserStatusChange(ide, loginuserr, statuse);
                    Console.ReadKey();
                    return this;
                case 9:
                    return Logout();
                default:
                    Environment.Exit(0);
                    break;
            }

            return new GuestRole(Db);
        }
    }
}
