﻿using System;
using System.Collections.Generic;
using System.Text;
using PhotoCLub.DataBaseAll;

namespace PhotoCLub
{
    public class GuestRole : Role
    {
        public new Guest CurrentRole { get; set; }
        public GuestRole(DataBase db)
        {
            CurrentRole = new Guest();
            ConnectionDB(db);
        }
        public Role RegisterNewUser(string name, string login, string password)
        {
            User newuser = Db.DataBaseUser.Register(name, login, password);
            if (newuser != null)
                return new UserRole(newuser, Db);
            return new GuestRole(Db);
        }
        public Role LoginUser(string login, string password)
        {
            ICustomer session = Db.DataBaseUser.Login(login, password);
            if (session != null)
            {
                if (session as User != null)
                {
                    return new UserRole(session as User, Db);
                }
                else if (session as Admin != null)
                {
                    return new AdminRole(session as Admin, Db);
                }
            }
            return new GuestRole(Db);
        }
        public override Role Menu()
        {

            Console.WriteLine("Welcome to our internet store PhotoCLub!");
            Console.WriteLine("Menu:");
            Console.WriteLine("1 - Пересмотреть товары");
            Console.WriteLine("2 - Поиск товара");
            Console.WriteLine("3 - Зарегистрироваться");
            Console.WriteLine("4 - Войти в систему");
            Console.WriteLine("5 - Закрыть сайт");
            string m = Console.ReadLine();
            if (m == "")
                return this;
            int p = Convert.ToInt32(m);
            switch (p)
            {
                case 1:
                    Console.WriteLine("| Name        | Price      | Category     |");
                    foreach (var element in ReturnAllProducts())
                    {
                        Console.WriteLine("| {0}    | {1}    | {2}    |",
                        element.Name, element.Price, element.Category);
                    }
                    Console.ReadKey();
                    return this;
                case 2:
                    Console.WriteLine("Введите название товара, который интересует");
                    string name = Console.ReadLine();
                    if (name == "")
                        return this;
                    ProductBase searchproduct = ReturnProductByName(name);
                    if (searchproduct != null)
                        Console.WriteLine("{0} - {1} - {2}",
                        searchproduct.Name,
                        searchproduct.Price,
                        searchproduct.Category);
                    else
                        Console.WriteLine("It isn't exist product with which name");
                    Console.ReadKey();
                    return this;
                case 3:
                    Console.WriteLine("Name:");
                    string nameRegister = Console.ReadLine();
                    Console.WriteLine("Login:");
                    string loginRegister = Console.ReadLine();
                    Console.WriteLine("Password:");
                    string passwordRegister = Console.ReadLine();
                    if (loginRegister == "" && passwordRegister == "" && nameRegister == "")
                        return this;
                    return RegisterNewUser(nameRegister, loginRegister, passwordRegister);
                case 4:
                    Console.WriteLine("Login:");
                    string login = Console.ReadLine();
                    Console.WriteLine("Password:");
                    string password = Console.ReadLine();
                    if (login == "" && password == "")
                        return this;
                    return LoginUser(login, password);
                default:
                    Environment.Exit(0);
                    break;
            }
            return new GuestRole(Db);
        }
    }
}
