﻿using System;
using System.Collections.Generic;
using System.Text;
using PhotoCLub.DataBaseAll;

namespace PhotoCLub
{
    public class UserRole : Role
    {
        public new User CurrentRole { get; set; }
        public UserRole(User currentrole, DataBase db)
        {
            CurrentRole = currentrole;
            ConnectionDB(db);
        }
        public Role Logout()
        {
            return new GuestRole(Db);
        }
        public void CreateNewOrder(string productname)
        {
            ProductBase ProductOrder = Db.DataBaseProduct.SearchByName(productname);
            Order NewOrder = new Order(ProductOrder);
            (CurrentRole).ListOfOrders.AddOrder(NewOrder);
        }
        public void StatusReceivedOrder(int id)
        {
            CurrentRole.ChangeStatusOfOrder(id, StatusType.Received);
        }
        public void StatusCanceledOrder(int id)
        {
            CurrentRole.ChangeStatusOfOrder(id, StatusType.CanceledByUser);
        }
        public UserRole ChangeInfoName(string newname)
        {
            return new UserRole(Db.DataBaseUser.ChangeInfo(CurrentRole,
                                new User(newname, (CurrentRole).Login, (CurrentRole).Password)) as User, Db);
        }
        public UserRole ChangeInfoLogin(string newlogin)
        {
            return new UserRole(Db.DataBaseUser.ChangeInfo(CurrentRole,
                                new User((CurrentRole).Name, newlogin, (CurrentRole).Password)) as User, Db);
        }
        public UserRole ChangeInfoPassword(string newpassword)
        {
            return new UserRole(Db.DataBaseUser.ChangeInfo(CurrentRole,
            new User((CurrentRole).Name, (CurrentRole).Login, newpassword)) as User, Db);
        }
        public List<Order> ReturnAllOrdersCurrentUSer()
        {
            List<Order> templist = new List<Order>();
            foreach (var element in CurrentRole.ListOfOrders)
                templist.Add(element.Item2);
            return templist;
        }
        public override Role Menu()
        {
            Console.WriteLine("User role");
            Console.WriteLine("Welcome to our internet store PhotoCLub, {0}!", (CurrentRole).Name);
            Console.WriteLine("Menu:");
            Console.WriteLine("1 - Пересмотреть товары");
            Console.WriteLine("2 - Поиск товара");
            Console.WriteLine("3 - Создать новый заказ");
            Console.WriteLine("4 - Jтменить заказ");
            Console.WriteLine("5 - Пересмотреть заказы и статус доставки");
            Console.WriteLine("6 - Уведомить о получении заказа");
            Console.WriteLine("7 - Изменить персональную информацию");
            Console.WriteLine("8 - Выход из системы");
            Console.WriteLine("9 - Закрыть сайт");
            string m = Console.ReadLine();
            if (m == "")
                return this;
            int p = Convert.ToInt32(m);
            switch (p)
            {
                case 1:
                    Console.WriteLine("| Name        | Price      | Category     |");
                    foreach (var element in ReturnAllProducts())
                    {
                        Console.WriteLine("| {0}    | {1}    | {2}    |",
                        element.Name, element.Price, element.Category);
                    }
                    Console.ReadKey();
                    return this;
                case 2:
                    Console.WriteLine("Введите название товара, который интересует");
                    string name = Console.ReadLine();
                    if (name == "")
                        return this;
                    if (ReturnProductByName(name) != null)
                        Console.WriteLine("{0} - {1} - {2}",
                        ReturnProductByName(name).Name,
                        ReturnProductByName(name).Price,
                        ReturnProductByName(name).Category);
                    else
                        Console.WriteLine("It isn't exist product with which name");
                    Console.ReadKey();
                    return this;
                case 3:
                    Console.WriteLine("Какой товар желаете заказать?");
                    string nameproduct = Console.ReadLine();
                    if (nameproduct == "")
                        return this;
                    CreateNewOrder(nameproduct);
                    Console.WriteLine("Order completely are created!");
                    Console.ReadKey();
                    return this;
                case 4:

                    if (CurrentRole.UserHaveAnyOrders())
                    {
                        Console.WriteLine("Your orders:");
                        foreach (var element in CurrentRole.ListOfOrders)
                            Console.WriteLine("{0} - {1} - {2} - {3}", element.Item1,
                                element.Item2.Product.Name,
                                element.Item2.Product.Price,
                                element.Item2.Product.Category);
                        Console.WriteLine("Which order by id would you cancel?");
                        string messege = Console.ReadLine();
                        if (messege == "")
                            return this;
                        int orderid = Convert.ToInt32(messege);
                        StatusCanceledOrder(orderid);
                        Console.WriteLine("Order succesful canceled!");
                        return this;
                    }
                    else
                        Console.WriteLine("You haven't any order");
                    return this;
                case 5:
                    if (CurrentRole.UserHaveAnyOrders())
                    {
                        Console.WriteLine("Your orders:");
                        foreach (var element in CurrentRole.ListOfOrders)
                            Console.WriteLine("{0} - {1} - {2} - {3} - {4}", element.Item1,
                                element.Item2.Product.Name,
                                element.Item2.Product.Price,
                                element.Item2.Product.Category,
                                element.Item2.Status);
                    }
                    else
                        Console.WriteLine("You haven't any order");
                    Console.ReadKey();
                    return this;
                case 6:
                    if (CurrentRole.UserHaveAnyOrders())
                    {
                        Console.WriteLine("Your orders:");
                        foreach (var element in CurrentRole.ListOfOrders)
                            Console.WriteLine("{0} - {1} - {2} - {3} - {4}", element.Item1,
                                element.Item2.Product.Name,
                                element.Item2.Product.Price,
                                element.Item2.Product.Category,
                                element.Item2.Status);
                        Console.WriteLine("Which order by id you have received?");
                        string messege = Console.ReadLine();
                        if (messege == "")
                            return this;
                        int orderid = Convert.ToInt32(messege);
                        StatusReceivedOrder(orderid);
                        Console.WriteLine("Order succesful received!");
                        return this;
                    }
                    else
                        Console.WriteLine("You haven't any order");
                    return this;
                case 7:
                    Console.WriteLine("1 - изменить имя");
                    Console.WriteLine("2 - изменить логин");
                    Console.WriteLine("3 - изменить пароль");
                    string schok = Console.ReadLine();
                    if (schok == "")
                        return this;
                    int punkt = Convert.ToInt32(schok);
                    switch (punkt)
                    {
                        case 1:
                            Console.WriteLine("Введите новое имя");
                            string newname = Console.ReadLine();
                            Console.WriteLine("Completely changed!");
                            return ChangeInfoName(newname);
                        case 2:
                            Console.WriteLine("Введите новый логин");
                            string newlogin = Console.ReadLine();
                            Console.WriteLine("Completely changed!");
                            return ChangeInfoLogin(newlogin);
                        case 3:
                            Console.WriteLine("Введите новый пароль");
                            string newpassword = Console.ReadLine();
                            Console.WriteLine("Completely changed!");
                            return ChangeInfoPassword(newpassword);
                        default:
                            return this;
                    }
                case 8:
                    return Logout();
                default:
                    Environment.Exit(0);
                    break;
            }
            return new GuestRole(Db);
        }
    }
}
