﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    /// <summary>
    /// Enum with some StatusType
    /// </summary>
    public enum StatusType
    {
        New,
        CanceledByAdmin,
        PaymentReceived,
        Sent,
        Completed,
        Received,
        CanceledByUser
    }
}
