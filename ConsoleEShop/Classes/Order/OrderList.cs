﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{

    public class OrderList : IEnumerable<Tuple<int, Order>>
    {
        public Dictionary<int, Order> list;
        public int Id;
        public OrderList()
        {
            list = new Dictionary<int, Order>();
            Id = 0;
        }
        public Order this[int id]
        {
            get
            {
                if (list.ContainsKey(id))
                    return list[id];
                throw new OrderException("This order doesn't exist");
            }
            set
            {
                if (list.ContainsKey(id))
                {
                    list[id] = value;
                }
                else
                    throw new OrderException("This order doesn't exist");
            }
        }
        public void AddOrder(Order order)
        {
            Id++;
            list.Add(Id, order);
        }
        public void RemoveOrder(Order order)
        {
            foreach (var element in list)
            {
                if (element.Value == order)
                    list.Remove(element.Key);
            }

        }
        public IEnumerator<Tuple<int, Order>> GetEnumerator()
        {
            foreach (var order in list)
                yield return new Tuple<int, Order>(order.Key, order.Value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
