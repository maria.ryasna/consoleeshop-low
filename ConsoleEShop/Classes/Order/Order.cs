﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class Order
    {
        public ProductBase Product { get; set; }
        public StatusType Status { get; set; }
        public Order()
        {
            Status = StatusType.New;
        }
        public Order(ProductBase product)
        {
            Status = StatusType.New;
            Product = product;
        }
        public void OrderStatusChange(StatusType status)
        {
            if (Status == StatusType.CanceledByAdmin || Status == StatusType.CanceledByUser || Status == StatusType.Received)
                throw new OrderException($"You can not change status {Status}");
            Status = status;
        }

    }
}
