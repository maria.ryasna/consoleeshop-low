﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public abstract class ProductBase
    {
        public string Name { get; set; }
        public double Price { get; set; }


        public CategoryType Category { get; set; }
        public ProductBase(string name, double price)
        {
            Name = name;
            Price = price;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}