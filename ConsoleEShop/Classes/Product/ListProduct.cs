﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class ListProduct : IEnumerable<ProductBase>
    {
        Dictionary<int, ProductBase> list;
        int Count { get; set; }
        public ListProduct()
        {
            list = new Dictionary<int, ProductBase>();
            Count = 0;
        }
        public ListProduct(ProductBase product)
        {
            list = new Dictionary<int, ProductBase>();
            Count++;
            list.Add(Count, product);
        }
        public ListProduct(IEnumerable<ProductBase> productlist)
        {
            if (productlist == null)
                throw new UserNullException("It's trying add null");
            list = new Dictionary<int, ProductBase>();
            foreach (var element in productlist)
            {
                if (element == null)
                    throw new UserNullException("It's trying add null user");
                Count++;
                list.Add(Count, element);
            }
        }
        public bool ProductExistByName(string name)
        {
            foreach (var product in list)
            {
                if (product.Value.Name == name)
                    return true;
            }
            return false;
        }
        public ProductBase SearchByName(string name)
        {
            foreach (var element in list)
                if (element.Value.Name == name)
                    return element.Value;
            throw new ProductNullException("This product doesn't exist or not enoug information about product");
        }
        public void AddProduct(ProductBase product)
        {
            foreach (var element in list)
                if (element.Value.Name == product.Name)
                    throw new ProductNullException("This name of product already exist");
            Count++;
            list.Add(Count, product);
        }
        public ProductBase this[int id]
        {
            get
            {
                if (list.ContainsKey(id))
                    return list[id];
                throw new UserNullException("This user doesn't exist");
            }
            set
            {
                if (list.ContainsKey(id))
                {
                    list[id] = value;
                }
                else
                    throw new ProductNullException("This product doesn't exist");
            }
        }
        public void ChangeInfo(ProductBase previous, ProductBase current)
        {

            if (previous.Name != current.Name && ProductExistByName(current.Name))
                throw new UserNullException("Login already exist");
            foreach (var element in list)
            {
                if (element.Value.Name == previous.Name)
                {
                    list[element.Key].Name = current.Name;
                    list[element.Key].Price = current.Price;
                    list[element.Key].Category = current.Category;
                }
            }
       }
        public IEnumerator<ProductBase> GetEnumerator()
        {
            foreach (var product in list)
                yield return product.Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
