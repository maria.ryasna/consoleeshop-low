﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class Camera : ProductBase
    {
        public Camera(string name, double price) : base(name, price)
        {
            Category = CategoryType.Camera;
        }
    }
}
