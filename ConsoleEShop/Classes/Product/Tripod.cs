﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class Tripod : ProductBase
    {
        public Tripod(string name, double price) : base(name, price)
        {
            Category = CategoryType.Tripod;
        }
    }
}
