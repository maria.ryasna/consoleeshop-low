﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class Len : ProductBase
    {
        public Len(string name, double price) : base(name, price)
        {
            Category = CategoryType.Len;
        }
    }
}
