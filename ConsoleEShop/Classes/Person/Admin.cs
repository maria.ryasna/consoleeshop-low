﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class Admin : IPerson, ICustomer
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// Empty constructor for admin
        /// </summary>
        public Admin()
        {
            throw new UserNullException("You couldn't create account without data");
        }
        /// <summary>
        /// Constructor for admin 
        /// </summary>
        public Admin(Admin user)
        {
            Name = user.Name;
            Login = user.Login;
            Password = user.Password;
        }
        /// <summary>
        /// Constructor for admin by name, login and password
        /// </summary>
        public Admin(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
        }
        /// <summary>
        /// Autorization for admin
        /// </summary>
        public bool Autorization(string login, string password)
        {
            if (this.Login == login && this.Password == password)
                return true;
            return false;
        }
        /// <summary>
        /// Check if current login already exists
        /// </summary>
        public bool LoginExist(string login)
        {
            if (this.Login == login)
                return true;
            return false;
        }
    }
}
