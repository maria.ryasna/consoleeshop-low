﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class ListUser : IEnumerable<ICustomer>
    {
        public Dictionary<int, ICustomer> list { get; set; }
        public int Count { get; set; }
        public ListUser()
        {
            list = new Dictionary<int, ICustomer>();
            Count = 0;
        }
        public ListUser(ICustomer user)
        {
            list = new Dictionary<int, ICustomer>();
            Count++;
            list.Add(Count, user);
        }
        public ListUser(IEnumerable<ICustomer> userlist)
        {
            if (userlist == null)
                throw new UserNullException("It's trying add null");
            list = new Dictionary<int, ICustomer>();
            foreach (var element in userlist)
            {
                if (element == null)
                    throw new UserNullException("It's trying add null user");
                Count++;
                list.Add(Count, element);
            }
        }
        public bool UserExistByLogin(string login)
        {
            foreach (var user in list)
            {
                if (user.Value.Login == login)
                    return true;
            }
            return false;
        }
        public ICustomer SearchByLogin(string login)
        {
            foreach (var element in list)
                if (element.Value.Login == login)
                    return element.Value;
            throw new UserNullException("This user doesn't exist or information was incorrect");
        }
        public void AddUser(ICustomer user)
        {
            Count++;
            list.Add(Count, user);
        }
        public ICustomer Login(string login, string password)
        {
            foreach (var element in list)
            {
                if (element.Value.Autorization(login, password))
                    return element.Value;
            }
            throw new UserNullException("Login or password incorrect");
        }
        public User Register(string name, string login, string password)
        {
            foreach (var element in list)
            {
                if (element.Value.LoginExist(login))
                    throw new UserNullException("Login already exist");
            }
            if (login == "" || password == "" || name == "")
                throw new UserNullException("Invalid");
            User newuser = new User(name, login, password);
            AddUser(newuser);
            return newuser;
        }
        public ICustomer ChangeInfo(ICustomer previous, ICustomer current)
        {

            if (previous.Login != current.Login)
                foreach (var element in list)
                {
                    if (element.Value.LoginExist(current.Login))
                        throw new UserNullException("Login already exist");
                }

            foreach (var element in list)
            {
                if (element.Value.Login == previous.Login)
                {
                    list[element.Key].Name = current.Name;
                    list[element.Key].Password = current.Password;
                    list[element.Key].Login = current.Login;
                    return list[element.Key];
                }
            }
            return current;
        }
        public ICustomer this[int id]
        {
            get
            {
                if (list.ContainsKey(id))
                    return list[id];
                throw new UserNullException("This user doesn't exist");
            }
            set
            {
                if (list.ContainsKey(id))
                {
                    list[id] = value;
                }
                else
                    throw new UserNullException("This user doesn't exist");
            }
        }
        public ICustomer UserUppdate(ICustomer current)
        {
            foreach (var element in list)
            {
                if (element.Value.Login == current.Login)
                {
                    list[element.Key] = current;
                    return element.Value;
                }
            }
            return null;
        }
        public void DeleteUser(string login)
        {
            foreach (var element in list)
            {
                if (element.Value.Login == login)
                {
                    list.Remove(element.Key);
                }
            }
        }
        public IEnumerator<ICustomer> GetEnumerator()
        {
            foreach (var user in list)
                yield return user.Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
