﻿using System;
using System.Collections.Generic;
using System.Text;


namespace PhotoCLub
{
    public class User : IPerson, ICustomer
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// List of orders some User, which default empty list
        /// </summary>
        public OrderList ListOfOrders = new OrderList();
        /// <summary>
        /// Empty constructor for User, which throw exception, if User try create User account without information
        /// </summary>
        public User()
        {
            throw new UserNullException("You couldn't create account without data");
        }
        /// <summary>
        /// Constructor for User
        /// </summary>
        public User(User user)
        {
            Name = user.Name;
            Login = user.Login;
            Password = user.Password;
        }
        /// <summary>
        /// Constructor for User by name, login and password
        /// </summary>
        public User(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
        }
        /// <summary>
        /// Autorization for admin
        /// </summary>
        public bool Autorization(string login, string password)
        {
            if (this.Login == login && this.Password == password)
                return true;
            return false;
        }
        /// <summary>
        /// Check if current login already exists
        /// </summary>
        public bool LoginExist(string login)
        {
            if (this.Login == login)
                return true;
            return false;
        }
        /// <summary>
        /// Status change for some order in ListOfOrders some User
        /// </summary>
        public void ChangeStatusOfOrder(int id, StatusType newstatus)
        {
            ListOfOrders[id].OrderStatusChange(newstatus);
        }
        /// <summary>
        /// Check if User have any orders
        /// </summary>
        public bool UserHaveAnyOrders()
        {
            foreach (var order in ListOfOrders)
            {
                if (order != null)
                    return true;
                return false;
            }
            return false;
        }
    }
}
