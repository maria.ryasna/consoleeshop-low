﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class UserNullException : Exception
    {
        public UserNullException(string message)
        : base(message)
        { }
    }
}
