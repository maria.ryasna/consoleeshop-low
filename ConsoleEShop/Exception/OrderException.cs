﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class OrderException : Exception
    {
        public OrderException(string message)
        : base(message)
        { }
    }
}
