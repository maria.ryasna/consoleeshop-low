﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub
{
    public class ProductNullException : Exception
    {
        public ProductNullException(string message)
        : base(message)
        { }
    }
}
