﻿using System;
using System.Collections.Generic;
using System.Linq;
using PhotoCLub.DataBaseAll;

namespace PhotoCLub
{

    public class Program
    {
        static void Main(string[] args)
        {
            DataBase repository = new DataBase();
            List<ICustomer> list = new List<ICustomer>
            {
                new User("Pavel", "pavl12", "12345"),
                new User("Serg", "serg23", "123456"),
                new User("Krok", "pavl12", "123432"),
                new User("Iryna", "irochka", "12323"),
                new User("Jack", "jc42", "14123"),
                new User("John", "john", "123j"),
                new Admin("Maria", "granddy", "123")
            };
            List<ProductBase> products = new List<ProductBase>
            {
               new Camera("Canon 4000d", 12999),
                new Camera("Canon 2000d", 18000),
                new Camera("Canon 850d", 25000),
                new Camera("Canon 6D Mark II", 42000),
                new Camera("Sony a7 III", 57000),
                new Len("Canon 50mm", 8000),
                new Len("Tamron 50-200mm", 23000),
                new Len("Canon 35mm", 9000),
                new Len("Tamron 50mm", 5000),
                new Tripod("RX-45", 1000)
            };
            repository.CreateDataUser(list);
            repository.CreateDataProduct(products);
            Role current = new GuestRole(repository);
            while (true)
            {
                current = current.Menu();
            }
        }
    }
}