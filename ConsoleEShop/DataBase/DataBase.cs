﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoCLub.DataBaseAll
{
    public class DataBase
    {

        public ListUser DataBaseUser { get; set; }
        public ListProduct DataBaseProduct { get; set; }
        public void CreateDataUser(IEnumerable<ICustomer> list)
        {
            DataBaseUser = new ListUser(list);
        }
        public void CreateDataProduct(IEnumerable<ProductBase> list)
        {
            DataBaseProduct = new ListProduct(list);
        }
    }
}
