using System;
using System.Collections.Generic;
using PhotoCLub;
using NUnit.Framework;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using PhotoCLub.DataBaseAll;

namespace TestStore
{
    [TestFixture]
    [DefaultFloatingPointTolerance(0.00001)]
    public class Tests
    {
        DataBase Db = new DataBase();
        List<ICustomer> list = new List<ICustomer>
            {
                new User("Pavel", "pavl12", "12345"),
                new User("Serg", "serg23", "123456"),
                new User("Krok", "pavl12", "123432"),
                new User("Iryna", "irochka", "12323"),
                new User("Jack", "jc42", "14123"),
                new User("John", "john", "123j"),
                new Admin("Maria", "granddy", "123")
            };
        List<ProductBase> products = new List<ProductBase>
            {
               new Camera("Canon 4000d", 12999),
                new Camera("Canon 2000d", 18000),
                new Camera("Canon 850d", 25000),
                new Camera("Canon 6D Mark II", 42000),
                new Camera("Sony a7 III", 57000),
                new Len("Canon 50mm", 8000),
                new Len("Tamron 50-200mm", 23000),
                new Len("Canon 35mm", 9000),
                new Len("Tamron 50mm", 5000),
                new Tripod("RX-45", 1000)
            };
        Role current;

        [SetUp]
        public void Setup()
        {
            Db.CreateDataUser(list);
            Db.CreateDataProduct(products);
            current = new GuestRole(Db);
        }

        [TestCase()]
        public void LogoutFromUser()
        {
            // arrange
            current = new UserRole(new User("Temp", "temp", "temp"), Db);
            // act
            current = (current as UserRole).Logout();
            // assert
            Assert.IsTrue(current as UserRole == null);
        }

        [TestCase()]
        public void LogoutFromAdmin()
        {
            // arrange
            current = new AdminRole(new Admin("Temp", "temp", "temp"), Db);
            // act
            current = (current as AdminRole).Logout();
            // assert
            Assert.IsTrue(current as AdminRole == null);
        }


        [TestCase("Ivan", "trk12", "123")]
        [TestCase("Ivan", "sergio99", "123")]
        public void RegisterNewUserInStore(string name, string login, string password)
        {
            //arrange
            current = new GuestRole(Db);
            User userexpected = new User(name, login, password);
            //act
            current = (current as GuestRole).RegisterNewUser(name, login, password);
            //assert
            Assert.AreEqual(userexpected.Name, (current as UserRole).CurrentRole.Name);
        }


        [TestCase("Ivan", "", "123")]
        [TestCase("", "Sergey", "123")]
        [TestCase("", "", "123")]
        [TestCase("", "Sergey", "")]
        public void RegisterUserWithIncorrectInformation(string name, string login, string password)
        {
            // arrange
            var expectedTypeError = typeof(UserNullException);
            // act
            Exception ex = Assert.Catch(() => Db.DataBaseUser.Register(name, login, password));
            // assert
            Assert.AreEqual(expectedTypeError, ex.GetType(),
                message: "Register method throw UserNullException if name, password or login incorrect");
        }



        [TestCase("Ivan", "pavl12", "123")]
        [TestCase("Pavel", "serg23", "123")]
        public void RegisterUserLoginAlreadyExists(string name, string login, string password)
        {
            // arrange
            var expectedTypeError = typeof(UserNullException);
            // act
            Exception ex = Assert.Catch(() => Db.DataBaseUser.Register(name, login, password));
            // assert
            Assert.AreEqual(expectedTypeError, ex.GetType(),
                message: "Register method throw UserNullException if login already exists");
        }


        [TestCase("Ivan", "i1", "123", "Canon 4000d")]
        [TestCase("Sergey", "s2", "123", "Sony a7 III")]
        public void LoginAndCreateNewOrder(string name, string login, string password, string nameproduct)
        {
            // arrange
            current = new GuestRole(Db);
            // act
            current = (current as GuestRole).RegisterNewUser(name, login, password);
            (current as UserRole).CreateNewOrder(nameproduct);
            // assert
            Assert.IsTrue((current as UserRole).CurrentRole.UserHaveAnyOrders());
        }

        [TestCase("Ivan", "ivan47", "123")]
        [TestCase("Sergey", "serg235", "123")]
        public void ReturnUserRoleByRegistration(string name, string login, string password)
        {
            // arrange
            current = new GuestRole(Db);
            // act
            current = (current as GuestRole).RegisterNewUser(name, login, password);
            // assert
            Assert.IsTrue(current as UserRole != null);
        }


        [TestCase("Canon 4000d", 1000, CategoryType.Camera)]
        [TestCase("Canon 50mm", 1000, CategoryType.Len)]
        public void AddAlreadyExistsProduct(string name, int price, CategoryType category)
        {
            // arrange
            var expectedTypeError = typeof(ProductNullException);
            current = new AdminRole(new Admin("admin", "admin", "admin"), Db);
            // act
            Exception ex = Assert.Catch(() => (current as AdminRole).AddNewProduct(name, price, category));
            // assert
            Assert.AreEqual(expectedTypeError, ex.GetType(),
                message: "Adding method throw ProdctNullException if product already exists");
        }



        [TestCase("Ivan", "iv56", "123", "Sergey")]
        [TestCase("Sergey", "Serg245", "123", "Ivan")]
        public void RegisterNewUserAndChangeInfoByAdmin(string name, string login, string password, string newname)
        {
            // arrange
            AdminRole admin = new AdminRole(new Admin("admin", "admin", "admin"), Db);
            current = new GuestRole(Db);
            User newuserinfo = new User(newname, login, password);
            // act
            current = (current as GuestRole).RegisterNewUser(name, login, password);
            admin.ChangeUserInfo((current as UserRole).CurrentRole, newuserinfo);
            // assert
            Assert.AreEqual(newuserinfo.Name, (current as UserRole).CurrentRole.Name);
        }

        [TestCase("Canon 3000d", 12000, CategoryType.Camera)]
        [TestCase("Canon 20-50mm", 12000, CategoryType.Len)]
        public void AddNewProductByAdmin(string name, int price, CategoryType category)
        {
            // arrange
            AdminRole admin = new AdminRole(new Admin("admin", "admin", "admin"), Db);
            // act
            admin.AddNewProduct(name, price, category);
            ProductBase findnewproduct = Db.DataBaseProduct.SearchByName(name);
            // assert
            Assert.AreEqual(name, findnewproduct.Name);
        }


        [TestCase("John", "john", "123j", "Johny")]
        public void LoginAndChangeNameByUser(string name, string login, string password, string newname)
        {
            // arrange
            current = new GuestRole(Db);
            // act
            current = (current as GuestRole).LoginUser(login, password);
            (current as UserRole).ChangeInfoName(newname);
            // assert
            Assert.AreEqual(newname, (current as UserRole).CurrentRole.Name);
        }


        [TestCase("Iryna", "irochka", "12323", "ira129")]
        [TestCase("Jack", "jc42", "14123", "serg566")]
        public void LoginAndChangeLoginByUser(string name, string login, string password, string newlogin)
        {
            // arrange
            current = new GuestRole(Db);
            // act
            current = (current as GuestRole).LoginUser(login, password);
            (current as UserRole).ChangeInfoLogin(newlogin);
            // assert
            Assert.AreEqual(newlogin, (current as UserRole).CurrentRole.Login);
        }
    }
}